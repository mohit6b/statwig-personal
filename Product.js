const instance = require('./ethereum/product');
const web3 = require('./web3');

class Product {
  constructor() {
   // this.getMilkData();
    this.data = [];
  }

  async getMilkData() {
      const data = await instance.methods.milkData().call();
      this.data.push(JSON.parse(web3.utils.hexToString(JSON.parse(data))));
  }

  async getProductCount() {
    console.log('instance', instance);
    const data = await instance.methods.getMilklistCount().call();
    console.log('product list count', data);
    return data;
  }

  async getProductList() {
    try{
      const product = new Product();

      const productListCount = await instance.methods.getMilklistCount().call();
      console.log('product list count', productListCount);
      const productList = await Promise.all(
        Array(parseInt(productListCount))
          .fill()
          .map((element, index) => {
            return instance.methods.milkList(index).call();
          }),
      );
      return productList;
    }catch(e){
        console.log(e);
    }
  }

  async createMilkProduct(df, harvestDate, harvestTime, productId){

        const DF = df;
        const harvestdate = harvestDate;
        const harvesttime = harvestTime;
        const productID = productId;
        console.log('instance', instance);

        try{
          const accounts = await web3.eth.getAccounts();
          if( DF && productID && harvesttime && harvestdate) {
            await instance.methods.createProduct( DF, harvestdate, harvesttime, productID ).send({
              from: accounts[0],
              gas: '1000000'
            });

          }else{
            console.log('Error while creating new product');
          }
        }catch(e){
            console.log(e);
        }

          const productListCount = await instance.methods.getMilklistCount().call();
          console.log('product list count', productListCount);
          const productList = await Promise.all(
            Array(parseInt(productListCount))
              .fill()
              .map((element, index) => {
                return instance.methods.milkList(index).call();
              }),
          );
          return productList;
      }
}

module.exports = Product;
