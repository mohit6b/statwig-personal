const SHA256 = require('crypto-js/sha256');
const CryptoJS = require('crypto-js');

class ChainUtil {

  static hash(data) {
    return SHA256(JSON.stringify(data)).toString();
  }

  static encrypt(data) {
    CryptoJS.AES.encrypt(JSON.stringify(data), 'secret key 123');
  }
}

module.exports = ChainUtil;