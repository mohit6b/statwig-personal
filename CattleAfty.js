const instance = require('./ethereum/cattleafty');
const web3 = require('./web3');

class CattleAfty {
  constructor() {
    this.data = [];
  }

  async getMilkData() {
      const data = await instance.methods.milkData().call();
      this.data.push(JSON.parse(web3.utils.hexToString(JSON.parse(data))));
  }

  async getCattleAftyCount() {
    console.log('instance', instance);
    const data = await instance.methods.getCattlelistCount().call();
    console.log('cattle list count', data);
    return data;
  }

  async getCattleAftyList() {
    try{

      const cattleAftyListCount = await instance.methods.getCattlelistCount().call();
      console.log('cattleafty list count', cattleAftyListCount);
      const cattleAftyList = await Promise.all(
        Array(parseInt(cattleAftyListCount))
          .fill()
          .map((element, index) => {
            return instance.methods.cattleList(index).call();
          }),
      );
      return cattleAftyList;

    }catch(e){
        console.log(e);
    }
  }

  async getParticularCattle(RFID){
    const RFId = RFID;

    const cattleAftyListCount = await instance.methods.getCattlelistCount().call();
    console.log('cattleafty list count', cattleAftyListCount);
    const cattleAftyList = await Promise.all(
      Array(parseInt(cattleAftyListCount))
        .fill()
        .map((element, index) => {
          return instance.methods.cattleList(index).call();
        }),
    );

    for(var index = 0; index < cattleAftyList.length; index++ ){
      if(cattleAftyList[index].RFID == RFId){
        const particularCattle = cattleAftyList[index].RFID + ',' + cattleAftyList[index].cattleBreed + ',' + cattleAftyList[index].treatmentDate + ',' + cattleAftyList[index].feedType +
                                ',' + cattleAftyList[index].feedStartDate + ',' + cattleAftyList[index].feedEndDate + ',' + cattleAftyList[index].finalDestination;

        return particularCattle;
      }else{
        return 'Product does not exist';
      }
    }

  }

    async createNewProduct(cattleBreed, treatmentDate, feedType, feedStartDate, feedEndDate, finalDestination, RFID){

          const cattlebreed = cattleBreed;
          const treatmentdate = treatmentDate;
          const feedtype = feedType;
          const feedstartdate = feedStartDate;
          const feedenddate = feedEndDate;
          const finaldestination = finalDestination;
          const RFId = RFID;
          console.log('instance', instance);

          try{
            const accounts = await web3.eth.getAccounts();
            if( cattlebreed  && RFId  ) {
              await instance.methods.createCustomer( cattlebreed, treatmentdate, feedtype, feedstartdate, feedenddate, finaldestination, RFId ).send({
                from: accounts[0],
                gas: '1000000'
              });
            }else{
              console.log('Error while creating new product');
            }
          }catch(e){
              console.log(e);
          }

            const cattleListCount = await instance.methods.getCattlelistCount().call();
            console.log('cattle list count', cattleListCount);
            const cattleAftyList = await Promise.all(
              Array(parseInt(cattleListCount))
                .fill()
                .map((element, index) => {
                  return instance.methods.cattleList(index).call();
                }),
            );
            return cattleAftyList;

        }

}

module.exports = CattleAfty;
