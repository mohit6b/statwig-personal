const instance = require('./ethereum/trackingdetails');
const web3 = require('./web3');

class TrackingDetails {
  constructor() {
    this.data = [];
  }

  async getMilkData() {
      const data = await instance.methods.milkData().call();
      this.data.push(JSON.parse(web3.utils.hexToString(JSON.parse(data))));
  }

  async getTrackingDetailsCount() {
    console.log('instance', instance);
    const data = await instance.methods.getTrackinglistCount().call();
    console.log('tracking details list count', data);
    return data;
  }

  async getTrackingDetailsList() {
    try{

      const trackingDetailsListCount = await instance.methods.getTrackinglistCount().call();
      console.log('tracking details list count', trackingDetailsListCount);
      const trackingDetailsList = await Promise.all(
        Array(parseInt(trackingDetailsListCount))
          .fill()
          .map((element, index) => {
            return instance.methods.trackingList(index).call();
          }),
      );
      return trackingDetailsList;

    }catch(e){
        console.log(e);
    }
  }

  async getParticularTrackingDetails(RFID){
    const RFId = RFID;

    const trackingDetailsListCount = await instance.methods.getTrackinglistCount().call();
    console.log('tracking details list count', trackingDetailsListCount);
    const trackingDetailsList = await Promise.all(
      Array(parseInt(trackingDetailsListCount))
        .fill()
        .map((element, index) => {
          return instance.methods.trackingList(index).call();
        }),
    );
    for(var index = 0; index < trackingDetailsList.length; index++ ){
      if(trackingDetailsList[index].RFID == RFId){
        const particularTrackingDetails = trackingDetailsList[index].dispatchWeight + ',' + trackingDetailsList[index].saleTimeDate
                              + ',' + trackingDetailsList[index].transferTimeDate + ',' + trackingDetailsList[index].transferStations
                              + ',' + trackingDetailsList[index].destinationTimeDate + ',' + trackingDetailsList[index].commentsIssues;
        return particularTrackingDetails;
      }else{
        return '';
      }
    }
  }


    async addNewTrackingDetails(dispatchWeight, saleTimeDate, transferTimeDate, transferStations, destinationTimeDate, commentsIssues, RFID){

          const dispatchweight = dispatchWeight;
          const saletimedate = saleTimeDate;
          const transfertimedate = transferTimeDate;
          const transferstations = transferStations;
          const destinationtimedate = destinationTimeDate;
          const commentsissues = commentsIssues;
          const RFId = RFID;
          console.log('instance', instance);

          try{
            const accounts = await web3.eth.getAccounts();
            if(dispatchweight  && RFId ) {
              await instance.methods.addTrackingDetail( dispatchWeight, saleTimeDate, transferTimeDate, transferStations, destinationTimeDate, commentsIssues, RFID ).send({
                from: accounts[0],
                gas: '1000000'
              });
            }else{
              console.log('Error while adding Tracking Details');
            }
          }catch(e){
              console.log(e);
          }

            const trackingdetailsListCount = await instance.methods.getTrackinglistCount().call();
            console.log('tracking details list count', trackingdetailsListCount);
            const trackingDetailsList = await Promise.all(
              Array(parseInt(cattleListCount))
                .fill()
                .map((element, index) => {
                  return instance.methods.trackingList(index).call();
                }),
            );
            return trackingDetailsList;

        }

}

module.exports = TrackingDetails;
