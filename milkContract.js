const web3 = require('./web3');
const MilkContract = require('./build/MilkContract.json');
const addresses = require('./build/addresses');

let instances = [];

addresses.forEach(address => {
  const instance = new web3.eth.Contract(
    JSON.parse(MilkContract.interface),
    address,
  );
  instances.push(instance);
});


module.exports = instances;
