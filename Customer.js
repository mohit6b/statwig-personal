const instance = require('./ethereum/customer');
const web3 = require('./web3');

class Customer {
  constructor() {
    this.data = [];
  }

  async getMilkData() {
      const data = await instance.methods.milkData().call();
      this.data.push(JSON.parse(web3.utils.hexToString(JSON.parse(data))));
  }

  async getCustomerCount() {
    console.log('instance', instance);
    const data = await instance.methods.getMilkCustomerCount().call();
    console.log('customer list count', data);
    return data;
  }

  async getCustomerList() {
    try{

      const customerListCount = await instance.methods.getMilkCustomerCount().call();
      console.log('customer list count', customerListCount);
      const customerList = await Promise.all(
        Array(parseInt(customerListCount))
          .fill()
          .map((element, index) => {
            return instance.methods.milkCustomerList(index).call();
          }),
      );
      return customerList;

    }catch(e){
        console.log(e);
    }
  }

    async createNewCustomer(arrivedDate, arrivedTime, productId){

          const arriveddate = arrivedDate;
          const arrivedtime = arrivedTime;
          const productID = productId;
          console.log('instance', instance);

          try{
            const accounts = await web3.eth.getAccounts();
            if( arriveddate  && arrivedtime && productID ) {
              await instance.methods.createCustomer( arriveddate, arrivedtime, productID ).send({
                from: accounts[0],
                gas: '1000000'
              });
            }else{
              console.log('Error while creating new product');
            }
          }catch(e){
              console.log(e);
          }

            const customerListCount = await instance.methods.getMilkCustomerCount().call();
            console.log('customer list count', customerListCount);
            const customerList = await Promise.all(
              Array(parseInt(customerListCount))
                .fill()
                .map((element, index) => {
                  return instance.methods.milkCustomerList(index).call();
                }),
            );
            return customerList;

        }

}

module.exports = Customer;
