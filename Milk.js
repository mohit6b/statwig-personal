const instances = require('./milkContract');
const web3 = require('./web3');

class Milk {
  constructor() {
    this.getMilkData();
    this.data = [];
  }

  async getMilkData() {
    for(let i=0; i<instances.length; i++) {
      const data = await instances[i].methods.milkData().call();
      this.data.push(JSON.parse(web3.utils.hexToString(JSON.parse(data))));
    }
  }
}

module.exports = Milk;