const express = require('express');
const bodyParser = require('body-parser');
const Milk = require('./Milk');
const Customer = require('./Customer');
const Product  = require('./Product');
const Distributor = require('./Distributor');
const CattleAfty = require('./CattleAfty');
const TrackingDetails = require('./TrackingDetails');
const Buyer = require('./Buyer');
const fs = require('fs');

const HTTP_PORT = process.env.HTTP_PORT || 3001;

const app = express();
const milk = new Milk();

app.use(bodyParser.json());

app.get('/blocks', (req, res) => {
  const key = Object.keys(req.query)[0];
  console.log('milk data', milk);
  let filteredData = milk.data;
  if (key) {
    filteredData = [];
    milk.data.forEach(items => {
      console.log('key', key);
      const found = items.find(item => {
        const itemKey = item[key];
        const queryKey = req.query[key];
        return item[key] === req.query[key];
      });
      filteredData.push(found);
    });
  }
  res.json(filteredData);
});

app.get('/CattleAfty/count', async (req, res) => {
  const cattleafty = new CattleAfty();
  const cattleaftyData = await cattleafty.getCattleAftyCount();
  console.log('cattleaftydata', cattleaftyData);
  res.json(cattleaftyData);
});

app.get('/CattleAfty/list', async (req, res) => {
  const cattleafty = new CattleAfty();
  const cattleaftyListData = await cattleafty.getCattleAftyList();
  console.log('cattleaftylistdata', cattleaftyListData);
  res.json(cattleaftyListData);
});

app.get('/cattle/:name', async (req, res) => {
  const cattleafty = new CattleAfty();
  const cattleData1 = await cattleafty.getParticularCattle(req.params.name);
  console.log('cattledata', cattleData1);
  const trackingdetails = new TrackingDetails();
  const cattleData2 = cattleData1 + ',' + await trackingdetails.getParticularTrackingDetails(req.params.name);
  console.log('cattledata', cattleData2);
  const buyer = new Buyer();
  const cattleData3 = cattleData2 + '' + await buyer.getParticularOrderDetails(req.params.name);
  console.log('cattledata', cattleData3);
  res.send(cattleData3);
});


app.post('/CattleAfty/add',  async(req, res) => {
  const cattleafty = new CattleAfty();
  const cattleafty_createData = await cattleafty.createNewProduct('', '', '', '', '24/05/2018', '2:00 PM', '9000');
  console.log('cattleafty_createdata', cattleafty_createData);
  res.json(cattleafty_createData);
});

app.get('/TrackingDetails/count', async (req, res) => {
  const trackingdetails = new TrackingDetails();
  const trackingData = await trackingdetails.getTrackingDetailsCount();
  console.log('trackingdata', trackingData);
  res.json(trackingData);
});

app.get('/TrackingDetails/list', async (req, res) => {
  const trackingdetails = new TrackingDetails();
  const trackingListData = await trackingdetails.getTrackingDetailsList();
  console.log('trackinglistdata', trackingListData);
  res.json(trackingListData);
});


app.post('/TrackingDetails/add',  async(req, res) => {
  const trackingdetails = new TrackingDetails();
  const trackingdetails_createData = await trackingdetails.addNewTrackingDetails('', '', '', '', '24/05/2018', '2:00 PM', '9000');
  console.log('trackingdetails_createdata', trackingdetails_createData);
  res.json(trackingdetails_createData);
});

app.get('/Order/count', async (req, res) => {
  const buyer = new Buyer();
  const orderData = await buyer.getOrderCount();
  console.log('orderdata', orderData);
  res.json(orderData);
});

app.get('/Order/list', async (req, res) => {
  const buyer = new Buyer();
  const orderListData = await buyer.getOrderList();
  console.log('orderlistdata', orderListData);
  res.json(orderListData);
});


app.post('/Order/add',  async(req, res) => {
  const buyer = new Buyer();
  const order_addData = await buyer.newOrderProduct('', '2:00 PM', '24/05/2018', '9000');
  console.log('order_adddata', order_addData);
  res.json(order_addData);
});


app.get('/Customer/count', async (req, res) => {
  const customer = new Customer();
  const customerData = await customer.getCustomerCount();
  console.log('customerdata', customerData);
  res.json(customerData);
});

app.get('/Customer/list', async (req, res) => {
  const customer = new Customer();
  const customerListData = await customer.getCustomerList();
  console.log('customerlistdata', customerListData);
  res.json(customerListData);
});


app.post('/Customer/create',  async(req, res) => {
  const customer = new Customer();
  const customer_createData = await customer.createNewCustomer('24/05/2018', '2:00 PM', '9000');
  console.log('customer_createdata', customer_createData);
  res.json(customer_createData);
});

app.get('/Product/count', async (req, res) => {
  const product = new Product();
  const productData = await product.getProductCount();
  console.log('productdata', productData);
  res.json(productData);
});

app.get('/Product/list', async (req, res) => {
  const product = new Product();
  const productListData = await product.getProductList();
  console.log('productlistdata', productListData);
  res.json(productListData);
});

app.post('/Product/create',  async(req, res) => {
  const product = new Product();
//  const dfAndroid, harvestdateAndroid, harvesttimeAndroid, productIdAndroid;
  const product_createData = await product.createMilkProduct(req.body.df, req.body.harvestDate, req.body.harvestTime, req.body.productId);
  console.log('product_createdata', product_createData);
  res.json(product_createData);
});

app.get('/Distributor/count', async (req, res) => {
  const distributor = new Distributor();
  const distributorData = await distributor.getDistributorCount();
  console.log('distributordata', distributorData);
  res.json(distributorData);
});

app.get('/Distributor/list', async (req, res) => {
  const distributor = new Distributor();
  const distributorListData = await distributor.getDistributorList();
  console.log('distributorlistdata', distributorListData);
  res.json(distributorListData);
});

app.post('/Distributor/create',  async(req, res) => {
  const distributor = new Distributor();
  const distributor_createData = await distributor.createNewDistributor('5:00 PM', '24/05/2018', '8:00 PM', '9:00 PM', '9000');
  console.log('distributor_createdata', distributor_createData);
  res.json(distributor_createData);
});

app.listen(HTTP_PORT, () => console.log(`Listening on ${HTTP_PORT}`));
