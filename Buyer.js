const instance = require('./ethereum/buyer');
const web3 = require('./web3');

class Buyer {
  constructor() {
    this.data = [];
  }

  async getMilkData() {
      const data = await instance.methods.milkData().call();
      this.data.push(JSON.parse(web3.utils.hexToString(JSON.parse(data))));
  }

  async getOrderCount() {
    console.log('instance', instance);
    const data = await instance.methods.getOrderlistCount().call();
    console.log('order list count', data);
    return data;
  }

  async getOrderList() {
    try{

      const buyerOrderListCount = await instance.methods.getOrderlistCount().call();
      console.log('order list count', buyerOrderListCount);
      const buyerOrderList = await Promise.all(
        Array(parseInt(buyerOrderListCount))
          .fill()
          .map((element, index) => {
            return instance.methods.orderList(index).call();
          }),
      );
      return buyerOrderList;

    }catch(e){
        console.log(e);
    }
  }

  async getParticularOrderDetails(RFID){
    const RFId = RFID;

    const orderListCount = await instance.methods.getOrderlistCount().call();
    console.log('order list count', orderListCount);
    const buyerOrderList = await Promise.all(
      Array(parseInt(orderListCount))
        .fill()
        .map((element, index) => {
          return instance.methods.orderList(index).call();
        }),
    );
    for(var index = 0; index < buyerOrderList.length; index++ ){
      if(buyerOrderList[index].RFID == RFId){
        const particularOrderDetails = buyerOrderList[index].purchaseQuantity + ',' + buyerOrderList[index].purchaseTime
                              + ',' + buyerOrderList[index].purchaseDate ;
        return particularOrderDetails;
      }else{
        return '';
      }
    }
  }


    async newOrderProduct(purchaseQuantity, purchaseTime, purchaseDate, RFID){

          const purchasequantity = purchaseQuantity;
          const purchasetime = purchaseTime;
          const purchasedate = purchaseDate;
          const RFId = RFID;
          console.log('instance', instance);

          try{
            const accounts = await web3.eth.getAccounts();
            if( purchasequantity  && RFId  ) {
              await instance.methods.orderProduct(purchasequantity, purchasetime, purchasedate, RFId ).send({
                from: accounts[0],
                gas: '1000000'
              });
            }else{
              console.log('Error while creating new Order');
            }
          }catch(e){
              console.log(e);
          }

            const orderListCount = await instance.methods.getOrderlistCount().call();
            console.log('order list count', orderListCount);
            const buyerOrderList = await Promise.all(
              Array(parseInt(orderListCount))
                .fill()
                .map((element, index) => {
                  return instance.methods.buyerOrderList(index).call();
                }),
            );
            return buyerOrderList;

        }

}

module.exports = Buyer;
