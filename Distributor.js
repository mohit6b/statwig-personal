const instance = require('./ethereum/distributor');
const web3 = require('./web3');

class Distributor {
  constructor() {
   // this.getMilkData();
    this.data = [];
  }

  async getMilkData() {
      const data = await instance.methods.milkData().call();
      this.data.push(JSON.parse(web3.utils.hexToString(JSON.parse(data))));
  }

  async getDistributorCount() {
    console.log('instance', instance);
    const data = await instance.methods.getDistributorlistCount().call();
    console.log('distributor list count', data);
    return data;
  }

  async getDistributorList() {
    try{
      const distributor = new Distributor();

      const distributorListCount = await instance.methods.getDistributorlistCount().call();
      console.log('distributor list count', distributorListCount);
      const distributorList = await Promise.all(
        Array(parseInt(distributorListCount))
          .fill()
          .map((element, index) => {
            return instance.methods.milkDistributorList(index).call();
          }),
      );
      return distributorList;
    }catch(e){
        console.log(e);
    }
  }

  async createNewDistributor( pickupTime, pickupDate, distributionArrival, distributionDeparture, productId){

        const pickuptime = pickupTime;
        const pickupdate = pickupDate;
        const distributionarrival = distributionArrival;
        const distributiondeparture = distributionDeparture;
        const productID = productId;
        console.log('instance', instance);

        try{
          const accounts = await web3.eth.getAccounts();
          if( pickuptime && pickupdate && distributionarrival && distributiondeparture && productID) {
            await instance.methods.createDistributor( pickuptime, pickupdate, distributionarrival,
              distributiondeparture, productID ).send({
              from: accounts[0],
              gas: '1000000'
            });
          }else{
            console.log('Error while creating new Distributor');
          }
        }catch(e){
            console.log(e);
        }

          const distributorListCount = await instance.methods.getDistributorlistCount().call();
          console.log('distributor list count', distributorListCount);
          const distributorList = await Promise.all(
            Array(parseInt(distributorListCount))
              .fill()
              .map((element, index) => {
                return instance.methods.milkDistributorList(index).call();
              }),
          );
          return distributorList;
      }
}

module.exports = Distributor;
